# LMEで分析

## Analysis of gaze

```{r}
library(lme4)
library(reshape)
library(plyr)
library(magrittr)

# Rstudioならprojectを作るほうが早いそうです
getwd()
setwd("/home/kishiyama/home/thesis/branching/result/data_main/csv")

data <- read.csv("./output.csv", header =T)
head(list.files())
word1 = list.files()[1]
data_name = "word1"
data_all = read.csv(paste("./",word1,sep=""), header =T)
head(data_all)
# バランスを確認
table(data_all$ParticipantName, data_all$Condition)
table(data_all$Condition)
data = data_all

# AOIの名前をマッピングします　
data$Target = ifelse(data$AOI == 1, as.character(data$Q2AOI1), "BackGround")
# NOTE: AQ2OI2 となっているのはミス。もうこのまま行きます。
data$Target = ifelse(data$AOI == 2, as.character(data$AQ2OI2), data$Target)
data$Target = ifelse(data$AOI == 3, as.character(data$Q2AOI3), data$Target)
data$Target = ifelse(data$AOI == 4, as.character(data$Q4AOI4), data$Target)
data$Target = ifelse(data$AOI == 5, as.character(data$AOI5), data$Target)
data$Target = ifelse(data$AOI == 6, as.character(data$AOI6), data$Target)
data$Target = ifelse(data$AOI == 7, as.character(data$AOI7), data$Target)
data$Target = ifelse(data$AOI == 8, as.character(data$AOI8), data$Target)

data <-data[order(data$ParticipantName, data$sound, data$GazeStart),]
head(data)

#onset時間点：どのonsetから切り揃えるでしょうか
# 4900から5300

onset_var <- 250
offset_var <- 500
span <- offset_var - onset_var
data$slapse <- data$GazeStart - onset_var
data$elapse <- data$GazeEnd - onset_var
head(data)

# 例： onset1からでしたら0,
# data$slapse <- data$GazeStart - 0
# data$elapse <- data$GazeEnd   - 0

# onset2からでしたらdata$onset2
# data$slapse <- data$GazeStart - data$onset2
# data$elapse <- data$GazeEnd - data$onset2

# そうしたら、onset以前の部分は負数になって、
# 後で時間区間の長さを調整する部分と合わせて0以下は0と入れ替えられて処理しないことになる

#  slapse elapse
#1   -824   -670
#2   -603   -397
#3      3    173
#4    177    680
#5    926   1523
#6   1220   1603

# onset内の時間区間の長さを調整
# 仮に onset の時間から引いたら、0スタートでnエンドになる。
# n はオンセットからの時間区間となる。
data$slapse <- ifelse(data$slapse < 0, 0, data$slapse)
data$elapse <- ifelse(data$elapse >= span, span, data$elapse)
head(data)
# data$slapse <- ifelse(data$slapse < onset_var, onset_var, data$slapse)
# data$elapse <- ifelse(data$elapse >= offset_var, offset_var, data$elapse)

# 上の場合、開始点は0 ,終了点は800 となる。

# 例：開始点は0,終了点は1000
# -> 0以下は0で入れ替え、1000以上は1000で入れ替えます
# data$slapse <- ifelse(data$slapse < 0, 0, data$slapse)
# data$elapse <- ifelse(data$elapse >= 1000, 1000, data$elapse)

#  slapse elapse
#1      0   -199
#2      0    -10
#3      3      3
#4    177    177
#5    926   1000
#6   1220   1000

# 時間内の比率を見るから、durだけを見ていい（開始点と終了点はもう無視していい）
# どれくらいの時間見ていたか、という点に興味が有る。
# わかりづらいけど、途中まで進まえると分かりやすいかもしれない。
data$dur <- data$elapse - data$slapse
# 万が一0以下（オーバーした区間）を除外
data$dur <- ifelse(data$dur < 0, 0, data$dur)
head(data)

# onset_var <- 250
# offset_var <- 600

# ここは要注意。
# data$slapse <- ifelse(data$slapse < 0, 0, data$slapse)
# data$elapse <- ifelse(data$elapse >= span, span, data$elapse)
#   AOI8 Q2TLB Q2TRB Q2DLB Q2DRB list no AOI     Target slapse elapse dur
# 1  DLB     5     2     8     6   B1  2   0 BackGround      0      3   3
# 2  DLB     5     2     8     6   B1  2   2        TRB     53    350 297
# 3  DLB     5     2     8     6   B1  2   2        TRB    445    350   0
# 4  DLB     5     2     8     6   B1  2   5        TLB    678    350   0

#必要なコラムだけを残ります
data <- data[,c("ParticipantName", "sound", "Target", "Condition", "slapse","elapse","dur")]
data <- data[order(data$ParticipantName,data$sound,data$slapse),]

# CALCULATING SUM (aggregation for each trial)
# 同じ区間の中もし別に他の注視時間があれば全部合算する。
head(data)
data <-aggregate(
    data$dur,
    by=list(data$ParticipantName, data$sound, 
        data$Target, data$Condition),
    FUN=sum,
    na.rm=TRUE)
colnames(data) = c("subj","item","AOI","cond","sum")

#sort
data <- data[order(data$subj, data$item),]
#"variable","value"に書き換えないと、cast()が実行できません
colnames(data) = c("subj","item","variable","cond","value")
head(data)
# cast creates separate columns for each object fixated
# 各絵に分けたら、それぞれの絵にどれくらい見ているのか、
# 後で分けて計算しやすい。（いちいち取り出すではなく、コラム単位で計算できる）

data2 <- cast(data)
head(data2)
# replace NULL
data2$BackGround <- ifelse(is.na(data2$BackGround), 0, data2$BackGround)
data2$DLB        <- ifelse(is.na(data2$DLB       ), 0, data2$DLB       )
data2$DRB        <- ifelse(is.na(data2$DRB       ), 0, data2$DRB       )
data2$TLB        <- ifelse(is.na(data2$TLB       ), 0, data2$TLB       )
data2$TRB        <- ifelse(is.na(data2$TRB       ), 0, data2$TRB       )
data2$filler     <- ifelse(is.na(data2$filler    ), 0, data2$filler    )

data2$logit <- log((data2$TLB + 0.5) / (data2$TRB + 0.5))

head(data2)
# 条件がC(N2がV1をしている絵)の場合

# 条件が２つ以上の絵の場合
#data2$Competitor_TargetCompound <- data2$CompetitorCompound + data2$TargetCompound
#data2$logit <- log((data2$Competitor_TargetCompound + 0.5) / (data2$all - data2$Competitor_TargetCompound + 0.5))

# 下位条件を付けます
# data2$reset <- ifelse(data2$cond == "R" | data2$cond == "D", 1, 0)
data2$reset <- ifelse(data2$cond == "R", 1, 0)
data2 %>% head

#中心化
data2$reset <- scale(data2$reset, scale=T)

#転換しやすいため
# data2$logit<-data2$logit_c
tapply(data2$logit, list(data2$reset), mean)

sum(data2[data2$cond == "R",]$logit)
sum(data2[data2$cond == "D",]$logit)
data2%>%head

m10 <- lme4::lmer(logit ~ reset + (1|subj) + (1|item), data = data2)
m00 <- lme4::lmer(logit ~ 1 + (1|subj) + (1|item), data = data2)

anova(m10, m00)

# m10wi <- lmer(logit ~ npi * aff + (1 + npi*aff |subj) + (1 + npi*aff |item), data = data2)
#
m00wi  <-  lmer(logit ~ npi * aff + (1|subj) + (1|item), data = data2)
m00woi  <-  lmer(logit ~ npi + aff + (1|subj) + (1|item), data = data2)
m00won  <-  lmer(logit ~ aff + npi:aff + (1|subj) + (1|item), data = data2)
m00woa  <-  lmer(logit ~ npi + npi:aff + (1|subj) + (1|item), data = data2)

summary(m00wi)

# npi
anova(m00wi, m00won)

# aff
anova(m00wi, m00woa)

# npi:aff(interaction)
anova(m00wi, m00woi)

# みねみんのスライドに
# backward stepwise の方法が書いてあったので参照。
#
# latex(m00, file='',booktabs=T,dcolumn=T)
# library(xtable)

```

